import sys
import ROOT

f = ROOT.TFile.Open('hist_qqH.root')
keys = [k.GetName() for k in f.GetListOfKeys()]

required_keys = ['qqH_pt_1', 'qqH_pt_2']

print('\n'.join(keys))
for required_key in required_keys:
    if not required_key in keys:
        print(f'Required key not found. {required_key}')
        sys.exit(1)

integral = f.qqH_pt_1.Integral()
if abs(integral - 20.353254) > 0.0001:
    print(f'Integral of qqH_pt_1 is different: {integral}')
    sys.exit(1)

